﻿using System.Collections.Generic;
using Common.Logging;
using NHibernate;
using NHibernate.Criterion;

namespace Triton.NHibernate.Model.Dao
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
	public abstract class NHibernateBaseDao<T>
	{
		private readonly ILog log = LogManager.GetLogger(typeof (NHibernateBaseDao<T>));
		private ISession session;

		public ISession Session
		{
			get
			{
				if (this.session == null) {
					//new up a session if the session has not been set
				    //this.session = NHibernateSessionProvider.Instance.GetSessionFactory().OpenSession();
					this.session = NHibernateSessionProvider.Instance.GetSession();
				}
				return this.session;
			}

			set { this.session = value; }
		}


		/// <summary>
		/// Get a collection of objects that match the specified example.
		/// </summary>
		/// <param name="example">Example of the type of object to retrieve.</param>
		/// <returns>IList of matched objects</returns>
		public virtual IList<T> Get(T example)
        {
            IList<T> result = null;
            using(var tx = this.Session.BeginTransaction()) 
            { 
                // execute code that uses the session 
    			result = this.Session.CreateCriteria(typeof (T)).Add(Example.Create(example)).List<T>();
                //tx.Commit(); 
            }

            return result;
        }


		/// <summary>
		/// Get the object by id.
		/// </summary>
		/// <param name="id">id of the object to retrieve.</param>
		/// <returns>object of type T</returns>
		public virtual T Get(object id)
        {
            T result = default(T);
            //using(var tx = this.Session.BeginTransaction()) 
            //{ 
                // execute code that uses the session 
                result = this.Session.Get<T>(id);
            //    tx.Commit(); 
            //}

            return result;
        }


		/// <summary>
		/// Save the object using Save.
		/// </summary>
		/// <param name="obj">Object to save.</param>
		public virtual void Save(T obj)
		{
			using (ITransaction tx = this.Session.BeginTransaction()) {
				try {
				    this.Session.Save(obj);
                    this.Session.Flush();
					tx.Commit();
				} catch (HibernateException ex) {
					tx.Rollback();
					this.log.Error(errorMessage => errorMessage("Problem with saving the object.", ex));
					throw;
				}
			}
		}

        /// <summary>
        /// Save the object using SaveOrUpdate.
        /// </summary>
        /// <param name="obj">Object to save.</param>
        public virtual void SaveOrUpdate(T obj)
        {
            using (ITransaction tx = this.Session.BeginTransaction()) {
                try {
                    this.Session.SaveOrUpdate(obj);
                    this.Session.Flush();
                    tx.Commit();
                } catch (HibernateException ex) {
                    tx.Rollback();
                    this.log.Error(errorMessage => errorMessage("Problem with saving the object.", ex));
                    throw;
                }
            }
        }

	    /// <summary>
	    /// Save the object using SaveOrUpdate.
	    /// </summary>
	    /// <param name="obj">Object to save.</param>
	    public virtual void Update(T obj)
	    {
	        using (ITransaction tx = this.Session.BeginTransaction()) {
	            try {
	                //this.Session.SaveOrUpdate(obj);
	                this.Session.Update(obj);
                    this.Session.Flush();
	                tx.Commit();
	            } catch (HibernateException ex) {
	                tx.Rollback();
	                this.log.Error(errorMessage => errorMessage("Problem with saving the object.", ex));
	                throw;
	            }
	        }
	    }


		/// <summary>
		/// Uses SaveOrUpdate to persist the objects to the database.
		/// </summary>
		/// <param name="objs">List of objects to persist.</param>
		public virtual void Save(IList<T> objs)
		{
			using (ITransaction tx = this.Session.BeginTransaction()) {
				try {
					foreach (T obj in objs) {
						this.Session.SaveOrUpdate(obj);
                        this.Session.Flush();
						tx.Commit();
					}
				} catch (HibernateException ex) {
					tx.Rollback();
					this.log.Error(errorMessage => errorMessage("Problem with saving the list of objects.", ex));
					throw;
				}
			}
		}


		/// <summary>
		/// Delete the object.
		/// </summary>
		/// <param name="obj">object to delete.</param>
		public virtual void Delete(T obj)
		{
			using (ITransaction tx = this.Session.BeginTransaction()) {
				try {
					this.Session.Delete(obj);
                    this.Session.Flush();
					tx.Commit();
				} catch (HibernateException ex) {
					tx.Rollback();
					this.log.Error(errorMessage => errorMessage("Problem with deleting an object.", ex));
					throw;
				}
			}
		}
	}
}