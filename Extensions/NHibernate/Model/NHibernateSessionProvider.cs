﻿using System;
using System.Web;
using Common.Logging;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Context;
using Triton.NHibernate.Extensions;
using Triton.Support.Session;
using NHibernateConfiguration = NHibernate.Cfg;

namespace Triton.NHibernate.Model
{
    /// <summary>
    /// 
    /// </summary>
	public class NHibernateSessionProvider
	{
		private const string SESSION_NHIBERNATE_SESSION = "NHibernateSupport_Session";
		private static readonly object threadLock = new object();
		private static NHibernateSessionProvider instance;
		private readonly ISessionFactory sessionFactory;
		private static readonly ILog Logger = LogManager.GetLogger<NHibernateSessionProvider>();
		//private ISession session;

		/// <summary>
		/// Initializes a new instance of the NHibernateSessionProvider class.
		/// </summary>
		private NHibernateSessionProvider()
		{
			Logger.Debug(debugMessage => debugMessage("Creating a new NHibernateSessionProvider."));
				
			this.sessionFactory = GetSessionFactoryInternal();
		}

        /// <summary>
        /// 
        /// </summary>
		public static NHibernateSessionProvider Instance
		{
			get
			{
				if (instance == null) {
					lock (threadLock) {
						if (instance == null) {
							instance = new NHibernateSessionProvider();
						}
					}
				}
				return instance;
			}
		}

        /// <summary>
        /// 
        /// </summary>
		public static void Reset()
		{
			lock (threadLock) {
				if (instance != null) {
					Logger.Debug(debugMessage => debugMessage("Call to destroy the session factory."));

					try {
						instance.sessionFactory.Close();
						instance.sessionFactory.Dispose();

						Logger.Debug(debugMessage => debugMessage("Session factory closed and disposed."));
					} catch (Exception ex) {
						Logger.Error(errorMessage => errorMessage("Error occurred when trying to destroy the NHibernateSessionProvider singleton.", ex));
					}
				}
				instance = null;
			}
		}


		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public ISession GetSession()
        {
            try
            {
                if (CurrentSessionContext.HasBind(this.sessionFactory))
                {
                    this.sessionFactory.GetCurrentSession();

                }
                else
                {
                    CreateSession();

                }
                // check to see if there is an open session already;
            }
            catch (Exception e)
            {
                // the http session is busted so just return an new session.
                return this.sessionFactory.OpenSession();
            }

            return this.sessionFactory.GetCurrentSession();
            //if (SessionStateProvider.GetSessionState()[SESSION_NHIBERNATE_SESSION] as ISession == null) {
            //	Logger.Debug(debugMessage => debugMessage("ISession was not found in the HttpSession, creating a new one."));

            //             CreateSession();
            //}
            //return SessionStateProvider.GetSessionState()[SESSION_NHIBERNATE_SESSION] as ISession;
        }

	    /// <summary>
	    /// 
	    /// </summary>
	    /// <returns></returns>
	    public ISessionFactory GetSessionFactory()
	    {
	        return this.sessionFactory;
	    }

		internal void DestroySession()
		{
            try
            {
                var session = this.sessionFactory.GetCurrentSession();
                var transaction = session.Transaction;
                if (transaction != null && transaction.IsActive)
                {
                    transaction.Commit();
                }
                session = CurrentSessionContext.Unbind(this.sessionFactory);
                session.Close();            }
            catch (Exception ex)
            {
                Logger.Error(errorMessage => errorMessage("Error occurred when trying to destroy the session.", ex));
            }

			//try {
			//	Logger.Debug(debugMessage => debugMessage("Call to destroy session."));

			//	if (SessionStateProvider.GetSessionState() != null) {
			//		ISession session = SessionStateProvider.GetSessionState()[SESSION_NHIBERNATE_SESSION] as ISession;
			//		if (session != null) {
			//			session.Close();
			//			session.Dispose();
			//			Logger.Debug(debugMessage => debugMessage("Session closed and disposed."));
			//		}
			//		SessionStateProvider.GetSessionState()[SESSION_NHIBERNATE_SESSION] = null;
			//	}
			//} catch (Exception ex) {
			//	Logger.Error(errorMessage => errorMessage("Error occurred when trying to destroy the session.", ex));
			//}
		}


		private void CreateSession()
        {
            CurrentSessionContext.Bind(this.sessionFactory.OpenSession());
            //Logger.Info("Falling back to session based provider");
            //SessionStateProvider.GetSessionState()[SESSION_NHIBERNATE_SESSION] = this.sessionFactory.OpenSession();
        }


		private ISessionFactory GetSessionFactoryInternal()
		{
            var config = new NHibernateConfiguration.Configuration();
            config.LinqToHqlGeneratorsRegistry<StringComparisonLinqtoHqlGeneratorsRegistry>();
            return config.Configure().BuildSessionFactory();
		}
	}
}