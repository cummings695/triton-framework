using System;

namespace Triton.Membership.Model
{
    [Serializable]
	public class Username
	{
		public virtual long? Id { get; set; }

		public virtual string Value { get; set; }

		public virtual long? Version { get; set; }
	}
}